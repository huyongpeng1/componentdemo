package com.newtype.aboutusmodule.fragment;

import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.newtype.aboutusmodule.R;
import com.newtype.aboutusmodule.R2;
import com.newtype.aboutusmodule.presenter.AboutUsPresenter;
import com.newtype.aboutusmodule.presenter.contract.AboutUsContract;
import com.newtype.basemodule.base.view.LazyFragment;
import com.newtype.basemodule.constants.ARouterManager;
import com.newtype.basemodule.interfaces.OnItemClickListener;
import com.newtype.basemodule.model.http.api.TradingService;
import com.newtype.basemodule.widget.TitleWidget;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

import butterknife.OnClick;
import okhttp3.OkHttpClient;


/**
 * Created by Popper on 2019/4/8.
 */
@Route(path = ARouterManager.AboutUsModule_AboutUsFragment)
public class AboutUsFragment extends LazyFragment<AboutUsPresenter> implements OnItemClickListener<String>, AboutUsContract.View/*, View.OnClickListener*/ {
    @BindView(R2.id.title)
    TitleWidget mTitle;
    @BindView(R2.id.textView)
    TextView mTextView;
//    private TitleWidget mTitle;
    private List<String> mTitleList;

    @Override
    protected void initView(View view) {
//        mTitle = view.findViewById(R.id.title);
        initTitle(null, R.string.about_us);
//        view.findViewById(R.id.textView).setOnClickListener(this);
    }

    @Override
    public AboutUsPresenter getPresentInstance() {
        return new AboutUsPresenter(getContext());
    }

    private void initTitle(ViewPager viewPager, int... strings) {
        if (mTitleList == null) {
            mTitleList = new ArrayList<>();
        }
        mTitleList.clear();
        for (int i = 0; i < strings.length; i++) {
            mTitleList.add(getString(strings[i]));
        }
        mTitle.setList(mTitleList, /*this,*/ viewPager);
    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void initData() {

    }

    @Override
    public int getLayoutId() {
        return R.layout.about_fragment_about_us;
    }

    @Override
    public void onItemClick(View view, String data, int position) {

    }

   /* @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.textView) {
            ARouter.getInstance().build(ARouterManager.LoginModule_LoginActivity).navigation();
        }
    }*/


    @OnClick({R2.id.textView})
    public void onViewClicked(View v) {
        int i = v.getId();
        if (i == R.id.textView) {
            ARouter.getInstance().build(ARouterManager.LoginModule_LoginActivity).navigation();
        }
    }
}
