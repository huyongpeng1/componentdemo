package com.newtype.aboutusmodule.presenter;

import android.content.Context;

import com.newtype.aboutusmodule.model.mode.AboutUsMode;
import com.newtype.aboutusmodule.presenter.contract.AboutUsContract;
import com.newtype.basemodule.base.presenter.RXPresenter;
import com.newtype.basemodule.model.http.api.TradingService;

/**
 * Created by Popper on 2019/4/9.
 */

public class AboutUsPresenter extends RXPresenter<AboutUsContract.View, AboutUsMode> implements AboutUsContract.Presenter {

    public AboutUsPresenter( Context context) {
        super( context);
    }

    @Override
    public AboutUsMode getModeInstance() {
        return new AboutUsMode();
    }
}
