package com.newtype.membercentermodule.fragment;

import android.support.v4.view.ViewPager;
import android.view.View;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.newtype.basemodule.constants.ARouterManager;
import com.newtype.basemodule.base.view.LazyFragment;
import com.newtype.basemodule.interfaces.OnItemClickListener;
import com.newtype.basemodule.model.http.api.TradingService;
import com.newtype.basemodule.widget.TitleWidget;
import com.newtype.membercentermodule.R;
import com.newtype.membercentermodule.presenter.MemberFragmentPresenter;
import com.newtype.membercentermodule.presenter.contract.MemberFragmentContract;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Popper on 2019/4/8.
 */
@Route(path = ARouterManager.MemberCenterModule_MemberFragment)
public class MemberFragment extends LazyFragment<MemberFragmentPresenter> implements OnItemClickListener<String>, MemberFragmentContract.View {
    private TitleWidget mTitle;
    private List<String> mTitleList;

    @Override
    protected void initView(View view) {
        mTitle = view.findViewById(R.id.title);
        initTitle(null, R.string.member_center);
    }

    @Override
    public MemberFragmentPresenter getPresentInstance() {
        return new MemberFragmentPresenter(getContext());
    }

    private void initTitle(ViewPager viewPager, int... strings) {
        if (mTitleList == null) {
            mTitleList = new ArrayList<>();
        }
        mTitleList.clear();
        for (int i = 0; i < strings.length; i++) {
            mTitleList.add(getString(strings[i]));
        }
        mTitle.setList(mTitleList,/* this,*/ viewPager);
    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void initData() {

    }

    @Override
    public int getLayoutId() {
        return R.layout.member_fragment_member;
    }

    @Override
    public void onItemClick(View view, String data, int position) {

    }
}
