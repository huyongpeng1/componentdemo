package com.newtype.membercentermodule.presenter;

import android.content.Context;

import com.newtype.basemodule.base.presenter.RXPresenter;
import com.newtype.basemodule.model.http.api.TradingService;
import com.newtype.membercentermodule.model.mode.MemberFragmentMode;
import com.newtype.membercentermodule.presenter.contract.MemberFragmentContract;

/**
 * Created by Popper on 2019/4/9.
 */

public class MemberFragmentPresenter extends RXPresenter<MemberFragmentContract.View, MemberFragmentMode> implements MemberFragmentContract.Presenter {

    public MemberFragmentPresenter( Context context) {
        super( context);
    }

    @Override
    public MemberFragmentMode getModeInstance() {
        return new MemberFragmentMode();
    }
}
