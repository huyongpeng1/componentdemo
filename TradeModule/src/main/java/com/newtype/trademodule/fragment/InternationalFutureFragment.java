package com.newtype.trademodule.fragment;

import android.view.View;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.newtype.basemodule.constants.ARouterManager;
import com.newtype.basemodule.base.view.LazyFragment;
import com.newtype.basemodule.model.http.api.TradingService;
import com.newtype.trademodule.R;
import com.newtype.trademodule.presenter.InternationalFutureFragmentPresenter;
import com.newtype.trademodule.presenter.contract.InternationalFutureFragmentContract;

/**
 * Created by Popper on 2019/4/8.
 */
@Route(path = ARouterManager.TradeModule_InternationalFutureFragment)
public class InternationalFutureFragment extends LazyFragment<InternationalFutureFragmentPresenter> implements InternationalFutureFragmentContract.View {
    @Override
    protected void initView(View view) {

    }

    @Override
    public InternationalFutureFragmentPresenter getPresentInstance() {
        return new InternationalFutureFragmentPresenter( getContext());
    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void initData() {

    }

    @Override
    public int getLayoutId() {
        return R.layout.trade_fragment_international_future;
    }
}
