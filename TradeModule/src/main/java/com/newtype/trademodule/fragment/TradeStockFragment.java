package com.newtype.trademodule.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.newtype.basemodule.constants.ARouterManager;
import com.newtype.basemodule.base.view.LazyFragment;
import com.newtype.basemodule.constants.Config;
import com.newtype.basemodule.model.http.api.TradingService;
import com.newtype.trademodule.R;
import com.newtype.trademodule.presenter.TradeStockFragmentPresenter;
import com.newtype.trademodule.presenter.contract.TradeStockFragmentContract;

/**
 * Created by Popper on 2019/4/8.
 */
@Route(path = ARouterManager.TradeModule_TradeStockFragment)
public class TradeStockFragment extends LazyFragment<TradeStockFragmentPresenter> implements TradeStockFragmentContract.View {

    private TextView mTextView;

    @Override
    protected void initView(View view) {
        mTextView = view.findViewById(R.id.textView);
    }

    @Override
    public TradeStockFragmentPresenter getPresentInstance() {
        return new TradeStockFragmentPresenter(getContext());
    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void initData() {
        Bundle arguments = getArguments();
        int type = arguments.getInt(Config.TRADE_STOCK_TYPE);
        mTextView.setText(0 == type ? "我是港股" : "我是美股");
    }

    @Override
    public int getLayoutId() {
        return R.layout.trade_fragment_trade_stock;
    }
}
