package com.newtype.trademodule.fragment;

import android.view.View;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.newtype.basemodule.constants.ARouterManager;
import com.newtype.basemodule.base.view.LazyFragment;
import com.newtype.basemodule.model.http.api.TradingService;
import com.newtype.trademodule.R;
import com.newtype.trademodule.presenter.ForexFragmentPresenter;
import com.newtype.trademodule.presenter.contract.ForexFragmentContract;

/**
 * Created by Popper on 2019/4/8.
 */
@Route(path = ARouterManager.TradeModule_ForexFragment)
public class ForexFragment extends LazyFragment<ForexFragmentPresenter> implements ForexFragmentContract.View {
    @Override
    protected void initView(View view) {

    }

    @Override
    public ForexFragmentPresenter getPresentInstance() {
        return new ForexFragmentPresenter( getContext());
    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void initData() {

    }

    @Override
    public int getLayoutId() {
        return R.layout.trade_fragment_forex;
    }
}
