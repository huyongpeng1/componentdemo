package com.newtype.trademodule.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.newtype.basemodule.constants.ARouterManager;
import com.newtype.basemodule.base.view.LazyFragment;
import com.newtype.basemodule.constants.Config;
import com.newtype.basemodule.interfaces.OnItemClickListener;
import com.newtype.basemodule.model.http.api.TradingService;
import com.newtype.basemodule.widget.TitleWidget;
import com.newtype.trademodule.R;
import com.newtype.trademodule.adapter.TradeViewPagerAdapter;
import com.newtype.trademodule.presenter.TradeContainerFragmentPresenter;
import com.newtype.trademodule.presenter.contract.TradeContainerFragmentContract;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Popper on 2019/4/8.
 */
@Route(path = ARouterManager.TradeModule_TradeContainerFragment)
public class TradeContainerFragment extends LazyFragment<TradeContainerFragmentPresenter> implements OnItemClickListener<String>, TradeContainerFragmentContract.View {


    private ArrayList<Fragment> mList;
    private ViewPager mViewPager;
    private TitleWidget mTitle;
    private List<String> mTitleList;

    @Override
    protected void initListener() {
        mTitle.setOnTitleClickListener(this);
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initView(View view) {
        mTitle = view.findViewById(R.id.title);
        mTitle.setMIndicatorPosition();
        mList = new ArrayList<>();
        mViewPager = view.findViewById(R.id.view_pager);
        Fragment hkStockFragment = (Fragment) ARouter.getInstance().build(ARouterManager.TradeModule_TradeStockFragment).navigation();
        Bundle bundle = new Bundle();
        bundle.putInt(Config.TRADE_STOCK_TYPE, 0);
        hkStockFragment.setArguments(bundle);
        mList.add(hkStockFragment);
        Fragment usaStockFragment = (Fragment) ARouter.getInstance().build(ARouterManager.TradeModule_TradeStockFragment).navigation();
        Bundle bundle1 = new Bundle();
        bundle1.putInt(Config.TRADE_STOCK_TYPE, 1);
        usaStockFragment.setArguments(bundle1);
        mList.add(usaStockFragment);
        mList.add((Fragment) ARouter.getInstance().build(ARouterManager.TradeModule_InternationalFutureFragment).navigation());
        mList.add((Fragment) ARouter.getInstance().build(ARouterManager.TradeModule_ForexFragment).navigation());
        TradeViewPagerAdapter tradeViewPagerAdapter = new TradeViewPagerAdapter(getChildFragmentManager(), mList);
        mViewPager.setAdapter(tradeViewPagerAdapter);
        initTitle(mViewPager, R.string.hk_stock, R.string.usa_stock, R.string.international_future, R.string.forex);
    }

    @Override
    public TradeContainerFragmentPresenter getPresentInstance() {
        return new TradeContainerFragmentPresenter( getContext());
    }

    private void initTitle(ViewPager viewPager, int... strings) {
        if (mTitleList == null) {
            mTitleList = new ArrayList<>();
        }
        mTitleList.clear();
        for (int i = 0; i < strings.length; i++) {
            mTitleList.add(getString(strings[i]));
        }
        mTitle.setList(mTitleList, /*this,*/ viewPager);
    }

    @Override
    public int getLayoutId() {
        return R.layout.trade_fragment_trade_container;
    }

    @Override
    public void onItemClick(View view, String data, int position) {

    }
}
