package com.newtype.trademodule.presenter;

import android.content.Context;

import com.newtype.basemodule.base.presenter.RXPresenter;
import com.newtype.basemodule.model.http.api.TradingService;
import com.newtype.trademodule.model.mode.InternationalFutureFragmentMode;
import com.newtype.trademodule.presenter.contract.InternationalFutureFragmentContract;

/**
 * Created by Popper on 2019/4/9.
 */

public class InternationalFutureFragmentPresenter extends RXPresenter<InternationalFutureFragmentContract.View, InternationalFutureFragmentMode> implements InternationalFutureFragmentContract.Presenter {

    public InternationalFutureFragmentPresenter( Context context) {
        super( context);
    }

    @Override
    public InternationalFutureFragmentMode getModeInstance() {
        return new InternationalFutureFragmentMode();
    }
}
