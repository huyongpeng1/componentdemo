package com.newtype.trademodule.presenter;

import android.content.Context;

import com.newtype.basemodule.base.presenter.RXPresenter;
import com.newtype.basemodule.model.http.api.TradingService;
import com.newtype.trademodule.model.mode.TradeContainerFragmentMode;
import com.newtype.trademodule.presenter.contract.TradeContainerFragmentContract;

/**
 * Created by Popper on 2019/4/9.
 */

public class TradeContainerFragmentPresenter extends RXPresenter<TradeContainerFragmentContract.View,TradeContainerFragmentMode>implements TradeContainerFragmentContract.Presenter {

    public TradeContainerFragmentPresenter( Context context) {
        super( context);
    }

    @Override
    public TradeContainerFragmentMode getModeInstance() {
        return new TradeContainerFragmentMode();
    }
}
