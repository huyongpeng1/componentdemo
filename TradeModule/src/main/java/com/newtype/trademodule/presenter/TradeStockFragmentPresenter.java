package com.newtype.trademodule.presenter;

import android.content.Context;

import com.newtype.basemodule.base.presenter.RXPresenter;
import com.newtype.basemodule.model.http.api.TradingService;
import com.newtype.trademodule.model.mode.TradeStockFragmentMode;
import com.newtype.trademodule.presenter.contract.TradeStockFragmentContract;

/**
 * Created by Popper on 2019/4/9.
 */

public class TradeStockFragmentPresenter extends RXPresenter<TradeStockFragmentContract.View, TradeStockFragmentMode> implements TradeStockFragmentContract.Presenter {

    public TradeStockFragmentPresenter( Context context) {
        super( context);
    }

    @Override
    public TradeStockFragmentMode getModeInstance() {
        return new TradeStockFragmentMode();
    }
}
