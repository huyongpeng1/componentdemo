package com.newtype.trademodule.presenter;

import android.content.Context;

import com.newtype.basemodule.base.presenter.RXPresenter;
import com.newtype.basemodule.model.http.RequestListener;
import com.newtype.basemodule.model.http.api.TradingService;
import com.newtype.trademodule.model.mode.ForexFragmentMode;
import com.newtype.trademodule.presenter.contract.ForexFragmentContract;

/**
 * Created by Popper on 2019/4/9.
 */

public class ForexFragmentPresenter extends RXPresenter<ForexFragmentContract.View, ForexFragmentMode> implements ForexFragmentContract.Presenter {

    public ForexFragmentPresenter(Context context) {
        super(context);
    }

    @Override
    public ForexFragmentMode getModeInstance() {
        return new ForexFragmentMode();
    }

}
