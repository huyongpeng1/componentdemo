package com.newtype.trademodule.presenter.contract;

import com.newtype.basemodule.base.view.BaseView;

/**
 * Created by Popper on 2019/4/9.
 */

public interface TradeContainerFragmentContract {
    interface View extends BaseView{}
    interface Presenter{}
}
