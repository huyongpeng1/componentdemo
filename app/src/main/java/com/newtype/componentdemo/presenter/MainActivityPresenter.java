package com.newtype.componentdemo.presenter;

import android.content.Context;

import com.newtype.basemodule.base.presenter.RXPresenter;
import com.newtype.basemodule.model.http.api.TradingService;
import com.newtype.componentdemo.model.mode.MainActivityMode;
import com.newtype.componentdemo.presenter.contract.MainActivityContract;


/**
 * Created by Popper on 2019/4/9.
 */

public class MainActivityPresenter extends RXPresenter<MainActivityContract.View, MainActivityMode> implements MainActivityContract.Presenter {

    public MainActivityPresenter( Context context) {
        super( context);
    }

    @Override
    public MainActivityMode getModeInstance() {
        return new MainActivityMode();
    }
}
