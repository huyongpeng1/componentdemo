package com.newtype.componentdemo.view.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.MenuItem;
import android.view.View;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.newtype.basemodule.base.view.BaseActivity;
import com.newtype.basemodule.constants.ARouterManager;
import com.newtype.basemodule.interfaces.OnItemClickListener;
import com.newtype.basemodule.model.http.api.TradingService;
import com.newtype.basemodule.utils.ToastUtil;
import com.newtype.componentdemo.R;
import com.newtype.componentdemo.presenter.MainActivityPresenter;
import com.newtype.componentdemo.presenter.contract.MainActivityContract;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * Created by Popper on 2019/4/3.
 */
@Route(path = ARouterManager.MainModule_MainActivity)
public class MainActivity extends BaseActivity<MainActivityPresenter> implements BottomNavigationView.OnNavigationItemSelectedListener, OnItemClickListener<Integer>, MainActivityContract.View {
    @BindView(R.id.bottom_navigation_view)
    BottomNavigationView mBottomNavigationView;
    private int mBottomSelectPosition;
    private ArrayList<Fragment> mContentList;
    private FragmentManager mSupportFragmentManager;
    private boolean isExit;
    private long mLastTime = System.currentTimeMillis();


    @Override
    protected void initInject() {
    }

    @Override
    protected void initListener() {
        mBottomNavigationView.setOnNavigationItemSelectedListener(this);
    }

    @Override
    protected void initData() {
        mContentList = new ArrayList<>();
        Fragment priceFragment = (Fragment) ARouter.getInstance().build(ARouterManager.PriceModule_PriceContainerFragment).navigation();
        mContentList.add(priceFragment);
        Fragment tradeFragment = (Fragment) ARouter.getInstance().build(ARouterManager.TradeModule_TradeContainerFragment).navigation();
        mContentList.add(tradeFragment);
        mContentList.add((Fragment) ARouter.getInstance().build(ARouterManager.MemberCenterModule_MemberFragment).navigation());
        mContentList.add((Fragment) ARouter.getInstance().build(ARouterManager.AboutUsModule_AboutUsFragment).navigation());
        getSupportFragmentManager().beginTransaction().add(R.id.frame_layout, priceFragment).show(priceFragment).commit();

    }


    private void switchFragment(int lastFragment, int index) {
        if (mSupportFragmentManager == null) {
            mSupportFragmentManager = getSupportFragmentManager();
        }
        FragmentTransaction transaction = mSupportFragmentManager.beginTransaction();
        transaction.hide(mContentList.get(lastFragment));//隐藏上个Fragment

        if (mContentList.get(index).isAdded() == false) {//如果该Fragment对象被添加到了它的Activity中，那么它返回true，否则返回false。
            transaction.add(R.id.frame_layout, mContentList.get(index));
        }
        transaction.show(mContentList.get(index)).commitAllowingStateLoss();

    }

    @Override
    protected void initView() {
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public MainActivityPresenter getPresentInstance() {
        return new MainActivityPresenter(this);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.navigation_price:
                if (mBottomSelectPosition != 0) {
                    switchFragment(mBottomSelectPosition, 0);
                    mBottomSelectPosition = 0;
                }
                return true;
            case R.id.navigation_trade:
                if (mBottomSelectPosition != 1) {
                    switchFragment(mBottomSelectPosition, 1);
                    mBottomSelectPosition = 1;
                }
                return true;
            case R.id.navigation_member:
                if (mBottomSelectPosition != 2) {
                    switchFragment(mBottomSelectPosition, 2);
                    mBottomSelectPosition = 2;
                }
                return true;
            case R.id.navigation_mine:
                if (mBottomSelectPosition != 3) {
                    switchFragment(mBottomSelectPosition, 3);
                    mBottomSelectPosition = 3;
                }
                return true;
        }
        return false;
    }

    @Override
    public void onItemClick(View view, Integer data, int position) {

    }

    @Override
    public void onBackPressed() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - mLastTime > 1200) {
            ToastUtil.showToastCenter(getString(R.string.press_again_to_exit));
        } else {
            Intent launcherIntent = new Intent(Intent.ACTION_MAIN);
            launcherIntent.addCategory(Intent.CATEGORY_HOME);
            startActivity(launcherIntent);
        }
        mLastTime = currentTime;

    }
}

