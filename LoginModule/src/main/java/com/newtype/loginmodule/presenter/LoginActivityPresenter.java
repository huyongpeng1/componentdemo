package com.newtype.loginmodule.presenter;

import android.content.Context;

import com.newtype.basemodule.base.presenter.RXPresenter;
import com.newtype.basemodule.model.http.api.TradingService;
import com.newtype.loginmodule.model.mode.LoginActivityMode;
import com.newtype.loginmodule.presenter.contract.LoginActivityContract;

/**
 * Created by Popper on 2019/4/9.
 */

public class LoginActivityPresenter extends RXPresenter<LoginActivityContract.View,LoginActivityMode> implements LoginActivityContract.Presenter {

    public LoginActivityPresenter( Context context) {
        super( context);
    }

    @Override
    public LoginActivityMode getModeInstance() {
        return new LoginActivityMode();
    }
}
