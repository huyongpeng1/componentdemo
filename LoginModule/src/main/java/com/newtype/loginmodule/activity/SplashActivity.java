package com.newtype.loginmodule.activity;

import android.os.Handler;
import android.view.View;

import com.alibaba.android.arouter.launcher.ARouter;
import com.newtype.basemodule.base.presenter.BasePresenter;
import com.newtype.basemodule.base.view.BaseActivity;
import com.newtype.basemodule.constants.ARouterManager;
import com.newtype.basemodule.utils.SystemUtil;
import com.newtype.basemodule.widget.DialogShow;
import com.newtype.loginmodule.R;
import com.newtype.loginmodule.R2;

import butterknife.OnClick;

/**
 * Created by Popper on 2019/4/2.
 */

public class SplashActivity extends BaseActivity {
    @Override
    protected void initInject() {

    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initView() {
        if (!SystemUtil.isNetworkConnected()) {
            DialogShow.showNetworkDialog(this);
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    ARouter.getInstance().build(ARouterManager.LoginModule_LoginActivity).navigation();
                    finish();
                }
            }, 1200);
        }
    }

    @Override
    public int getLayoutId() {
        return R.layout.login_activity_splash;
    }

    @Override
    public BasePresenter getPresentInstance() {
        return null;
    }


    @OnClick({R2.id.btn_jump_2_main_activity, R2.id.btn_login})
    public void onViewClicked(View view) {
        int i = view.getId();
        if (i == R.id.btn_jump_2_main_activity) {
            ARouter.getInstance().build(ARouterManager.MainModule_MainActivity).navigation();
        } else if (i == R.id.btn_login) {
            ARouter.getInstance().build(ARouterManager.LoginModule_LoginActivity).navigation();
        }
    }
}
