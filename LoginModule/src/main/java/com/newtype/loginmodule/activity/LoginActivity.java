package com.newtype.loginmodule.activity;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.newtype.basemodule.base.view.BaseActivity;
import com.newtype.basemodule.constants.ARouterManager;
import com.newtype.basemodule.interfaces.OnItemClickListener;
import com.newtype.basemodule.model.http.api.TradingService;
import com.newtype.basemodule.utils.ToastUtil;
import com.newtype.basemodule.widget.DialogShow;
import com.newtype.loginmodule.R;
import com.newtype.loginmodule.R2;
import com.newtype.loginmodule.presenter.LoginActivityPresenter;
import com.newtype.loginmodule.presenter.contract.LoginActivityContract;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Popper on 2019/4/9.
 */
@Route(path = ARouterManager.LoginModule_LoginActivity)
public class LoginActivity extends BaseActivity<LoginActivityPresenter> implements LoginActivityContract.View,/* View.OnClickListener,*/ OnItemClickListener<Integer> {

    @BindView(R2.id.iv_back)
    ImageView mIvBack;
    @BindView(R2.id.tv_phone_type)
    TextView mTvPhoneType;
    @BindView(R2.id.ll_phone_type)
    LinearLayout mLlPhoneType;
    @BindView(R2.id.btn_login)
    Button mBtnLogin;

    @Override
    protected void initInject() {

    }

    @Override
    protected void initListener() {
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initView() {
    }

    @Override
    public int getLayoutId() {
        return R.layout.login_activity_login;
    }

    @Override
    public LoginActivityPresenter getPresentInstance() {
        return new LoginActivityPresenter(this);
    }

    @OnClick({R2.id.ll_phone_type, R2.id.iv_back, R2.id.btn_login})
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.ll_phone_type) {
            DialogShow.showSelectPhoneTypeDialog(LoginActivity.this, mLlPhoneType, this);
        } else if (i == R.id.iv_back) {
            finish();
        } else if (i == R.id.btn_login) {
            ARouter.getInstance().build(ARouterManager.MainModule_MainActivity).navigation();
            finish();

        }
    }

    @Override
    public void onItemClick(View view, Integer data, int position) {
        int i = view.getId();//Library 不让使用switch，因为资源id没有加final
        if (i == R.id.ll_chinese_mainland) {

        } else if (i == R.id.ll_chinese_hk) {

        }
    }

}
