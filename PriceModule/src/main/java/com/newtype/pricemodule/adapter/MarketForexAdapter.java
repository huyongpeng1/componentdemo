package com.newtype.pricemodule.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.newtype.pricemodule.R;
import com.newtype.pricemodule.model.javabean.bean.MarketForexBean;

import java.util.List;

/**
 * Created by Popper on 2019/4/8.
 */

public class MarketForexAdapter extends BaseQuickAdapter<MarketForexBean, BaseViewHolder> {

    public MarketForexAdapter( @Nullable List<MarketForexBean> data) {
        super(R.layout.price_view_market_forex, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, MarketForexBean item) {

    }
}
