package com.newtype.pricemodule.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Created by Popper on 2019/4/8.
 */

public class PriceContainerAdapter extends FragmentPagerAdapter {
    public List<Fragment> mList;

    public PriceContainerAdapter(FragmentManager fm, List<Fragment> list) {
        super(fm);
        mList = list;
    }


    @Override
    public Fragment getItem(int i) {
        return mList.get(i);
    }

    @Override
    public int getCount() {
        return mList == null ? 0 : mList.size();
    }
}
