package com.newtype.pricemodule.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.newtype.pricemodule.R;
import com.newtype.pricemodule.model.javabean.bean.OptionalBean;

import java.util.List;

/**
 * Created by Popper on 2019/4/7.
 */

public class OptionalAdapter extends BaseQuickAdapter<OptionalBean,BaseViewHolder> {


    public OptionalAdapter( @Nullable List<OptionalBean> data) {
        super(R.layout.price_view_optional, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, OptionalBean item) {

    }
}
