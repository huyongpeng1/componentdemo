package com.newtype.pricemodule.view.fragment;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.newtype.basemodule.constants.ARouterManager;
import com.newtype.basemodule.base.view.LazyFragment;
import com.newtype.pricemodule.R;
import com.newtype.pricemodule.adapter.OptionalAdapter;
import com.newtype.pricemodule.model.javabean.bean.OptionalBean;
import com.newtype.pricemodule.presenter.OptionalContentFragmentPresenter;
import com.newtype.pricemodule.presenter.contract.OptionalContentFragmentContract;

import java.util.ArrayList;

/**
 * Created by Popper on 2019/4/7.
 */
@Route(path = ARouterManager.PriceModule_OptionalContentFragment)
public class OptionalContentFragment extends LazyFragment<OptionalContentFragmentPresenter> implements OptionalContentFragmentContract.View {

    private RecyclerView mRecyclerView;

    @Override
    public int getLayoutId() {
        return R.layout.price_fragment_optional_content;
    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void initData() {
        Log.d("way", "initData: 执行了");
        ArrayList<OptionalBean> list = new ArrayList<>();
        list.add(new OptionalBean());
        list.add(new OptionalBean());
        list.add(new OptionalBean());
        OptionalAdapter optionalAdapter = new OptionalAdapter(list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        optionalAdapter.bindToRecyclerView(mRecyclerView);
    }

    @Override
    protected void initView(View view) {
        mRecyclerView = view.findViewById(R.id.recycler_view);
    }

    @Override
    public OptionalContentFragmentPresenter getPresentInstance() {
        return new OptionalContentFragmentPresenter( getContext());
    }
}
