package com.newtype.pricemodule.view.fragment;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.newtype.basemodule.constants.ARouterManager;
import com.newtype.basemodule.base.view.LazyFragment;
import com.newtype.basemodule.constants.Config;
import com.newtype.basemodule.model.http.api.TradingService;
import com.newtype.pricemodule.R;
import com.newtype.pricemodule.adapter.OptionalViewPagerAdapter;
import com.newtype.pricemodule.presenter.MarketContainerFragmentPresenter;
import com.newtype.pricemodule.presenter.contract.MarketContainerFragmentContract;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.ColorTransitionPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.SimplePagerTitleView;

import java.util.ArrayList;

/**
 * Created by Popper on 2019/4/8.
 */
@Route(path = ARouterManager.PriceModule_MarketContainerFragment)
public class MarketContainerFragment extends LazyFragment<MarketContainerFragmentPresenter> implements MarketContainerFragmentContract.View {

    private ViewPager mViewPager;
    private ArrayList<String> mList;
    private MagicIndicator mMagicIndicator;

    @Override
    protected void initView(View view) {
        mViewPager = view.findViewById(R.id.view_pager);
        mMagicIndicator = view.findViewById(R.id.mIndicator);
    }

    @Override
    public MarketContainerFragmentPresenter getPresentInstance() {
        return new MarketContainerFragmentPresenter( getContext());
    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void initData() {
        mList = new ArrayList<>();
        mList.add(getString(R.string.hk_stock));
        mList.add(getString(R.string.usa_stock));
        mList.add(getString(R.string.hk_future));
        mList.add(getString(R.string.international_future));
        mList.add(getString(R.string.forex_market));
        ArrayList<Fragment> fragmentList = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            Fragment fragment = (Fragment) ARouter.getInstance().build(ARouterManager.PriceModule_MarketHKOrUSAStockFragment).navigation();
            Bundle bundle = new Bundle();
            bundle.putInt(Config.MARKET_STOCK_TYPE, i);
            fragment.setArguments(bundle);
            fragmentList.add(fragment);
        }

        for (int i = 0; i < 2; i++) {
            Fragment futureFragment = (Fragment) ARouter.getInstance().build(ARouterManager.PriceModule_MarketHKOrUSAFutureFragment).navigation();
            Bundle bundle = new Bundle();
            bundle.putInt(Config.MARKET_FUTURE_TYPE, i);
            futureFragment.setArguments(bundle);
            fragmentList.add(futureFragment);
        }
        fragmentList.add((Fragment) ARouter.getInstance().build(ARouterManager.PriceModule_MarketForexFragment).navigation());
        initMagicIndicator();
        OptionalViewPagerAdapter optionalViewPagerAdapter = new OptionalViewPagerAdapter(getChildFragmentManager(), fragmentList);
        mViewPager.setAdapter(optionalViewPagerAdapter);
    }

    private void initMagicIndicator() {
        CommonNavigator commonNavigator = new CommonNavigator(getContext());
//        commonNavigator.setAdjustMode(true);
        commonNavigator.setSkimOver(true);
        commonNavigator.setAdapter(new CommonNavigatorAdapter() {
            @Override
            public int getCount() {
                return mList == null ? 0 : mList.size();
            }

            @Override
            public IPagerTitleView getTitleView(Context context, final int index) {
                SimplePagerTitleView simplePagerTitleView = new ColorTransitionPagerTitleView(context);
                simplePagerTitleView.setNormalColor(Color.parseColor("#8E9AB6"));
                simplePagerTitleView.setSelectedColor(Color.parseColor("#3891FF"));
                simplePagerTitleView.setText(mList.get(index));
                simplePagerTitleView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mViewPager.setCurrentItem(index);
                        mMagicIndicator.onPageSelected(index);
                        mMagicIndicator.onPageScrolled(index, 0.0f, 0);
                    }
                });
                return simplePagerTitleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                LinePagerIndicator linePagerIndicator = new LinePagerIndicator(context);
                linePagerIndicator.setMode(LinePagerIndicator.MODE_WRAP_CONTENT);
                linePagerIndicator.setColors(Color.parseColor("#3891FF"));
                return linePagerIndicator;
            }
        });
        mMagicIndicator.setNavigator(commonNavigator);
        ViewPagerHelper.bind(mMagicIndicator, mViewPager);
    }

    @Override
    public int getLayoutId() {
        return R.layout.price_fragment_market_container;
    }
}
