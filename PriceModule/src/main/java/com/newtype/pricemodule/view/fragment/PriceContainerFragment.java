package com.newtype.pricemodule.view.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.newtype.basemodule.base.view.BaseFragment;
import com.newtype.basemodule.base.view.LazyFragment;
import com.newtype.basemodule.constants.ARouterManager;
import com.newtype.basemodule.interfaces.OnItemClickListener;
import com.newtype.basemodule.model.http.api.TradingService;
import com.newtype.basemodule.widget.TitleWidget;
import com.newtype.pricemodule.R;
import com.newtype.pricemodule.adapter.PriceContainerAdapter;
import com.newtype.pricemodule.presenter.PriceContainerFragmentPresenter;
import com.newtype.pricemodule.presenter.contract.PriceContainerFragmentContract;

import java.util.ArrayList;

/**
 * Created by Popper on 2019/4/8.
 */
@Route(path = ARouterManager.PriceModule_PriceContainerFragment)
public class PriceContainerFragment extends LazyFragment<PriceContainerFragmentPresenter> implements OnItemClickListener<String>, PriceContainerFragmentContract.View {

    private TitleWidget mTitle;
    private ViewPager mViewPager;
    private ArrayList<String> mTitleList;

    @Override
    protected void initView(View view) {
        mTitle = view.findViewById(R.id.title);
        mViewPager = view.findViewById(R.id.view_pager);
        ArrayList<Fragment> fragmentList = new ArrayList<>();
        fragmentList.add((Fragment) ARouter.getInstance().build(ARouterManager.PriceModule_OptionalFragment).navigation());
        fragmentList.add((Fragment) ARouter.getInstance().build(ARouterManager.PriceModule_MarketContainerFragment).navigation());
        PriceContainerAdapter priceContainerAdapter = new PriceContainerAdapter(getChildFragmentManager(), fragmentList);
        mViewPager.setAdapter(priceContainerAdapter);
        initTitle(mViewPager, R.string.optional, R.string.market);

    }

    @Override
    public PriceContainerFragmentPresenter getPresentInstance() {
        return new PriceContainerFragmentPresenter(getContext());
    }

    private void initTitle(ViewPager viewPager, int... strings) {
        if (mTitleList == null) {
            mTitleList = new ArrayList<>();
        }
        mTitleList.clear();
        for (int i = 0; i < strings.length; i++) {
            mTitleList.add(getString(strings[i]));
        }
        mTitle.setList(mTitleList, /*this,*/ viewPager);
    }

    @Override
    protected void initListener() {
        mTitle.setOnTitleClickListener(this);
    }

    @Override
    protected void initData() {
    }

    @Override
    public int getLayoutId() {
        return R.layout.price_fragment_price_container;
    }

    @Override
    public void onItemClick(View view, String data, int position) {

    }
}
