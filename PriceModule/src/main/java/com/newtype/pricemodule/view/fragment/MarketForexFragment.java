package com.newtype.pricemodule.view.fragment;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.newtype.basemodule.constants.ARouterManager;
import com.newtype.basemodule.base.view.LazyFragment;
import com.newtype.pricemodule.R;
import com.newtype.pricemodule.adapter.MarketForexAdapter;
import com.newtype.pricemodule.model.javabean.bean.MarketForexBean;
import com.newtype.pricemodule.presenter.MarketForexFragmentPresenter;
import com.newtype.pricemodule.presenter.contract.MarketForexFragmentContract;

import java.util.ArrayList;

/**
 * Created by Popper on 2019/4/8.
 */
@Route(path = ARouterManager.PriceModule_MarketForexFragment)
public class MarketForexFragment extends LazyFragment<MarketForexFragmentPresenter> implements MarketForexFragmentContract.View {
    @Override
    protected void initView(View view) {
        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        ArrayList<MarketForexBean> list = new ArrayList<>();
        list.add(new MarketForexBean());
        list.add(new MarketForexBean());
        list.add(new MarketForexBean());
        list.add(new MarketForexBean());
        MarketForexAdapter marketForexAdapter = new MarketForexAdapter(list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(marketForexAdapter);
    }

    @Override
    public MarketForexFragmentPresenter getPresentInstance() {
        return new MarketForexFragmentPresenter( getContext());
    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void initData() {

    }

    @Override
    public int getLayoutId() {
        return R.layout.price_fragment_market_forex;
    }
}
