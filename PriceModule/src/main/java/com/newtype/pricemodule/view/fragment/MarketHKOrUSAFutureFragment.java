package com.newtype.pricemodule.view.fragment;

import android.view.View;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.newtype.basemodule.constants.ARouterManager;
import com.newtype.basemodule.base.view.LazyFragment;
import com.newtype.basemodule.constants.Config;
import com.newtype.basemodule.model.http.api.TradingService;
import com.newtype.pricemodule.R;
import com.newtype.pricemodule.presenter.MarketHKOrUSAFutureFragmentPresenter;
import com.newtype.pricemodule.presenter.contract.MarketHKOrUSAFutureFragmentContract;

/**
 * Created by Popper on 2019/4/8.
 */
@Route(path = ARouterManager.PriceModule_MarketHKOrUSAFutureFragment)
public class MarketHKOrUSAFutureFragment extends LazyFragment<MarketHKOrUSAFutureFragmentPresenter> implements MarketHKOrUSAFutureFragmentContract.View {
    @Override
    protected void initView(View view) {
        TextView textView = view.findViewById(R.id.textView);
        int type = getArguments().getInt(Config.MARKET_FUTURE_TYPE);
        textView.setText(0 == type ? "这个是香港期货的页面" : "这个是国际期货的页面");
    }

    @Override
    public MarketHKOrUSAFutureFragmentPresenter getPresentInstance() {
        return new MarketHKOrUSAFutureFragmentPresenter( getContext());
    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void initData() {

    }

    @Override
    public int getLayoutId() {
        return R.layout.price_fragment_market_hk_or_usa_future;
    }
}
