package com.newtype.pricemodule.view.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.newtype.basemodule.constants.ARouterManager;
import com.newtype.basemodule.base.view.LazyFragment;
import com.newtype.basemodule.constants.Config;
import com.newtype.basemodule.model.http.api.TradingService;
import com.newtype.pricemodule.R;
import com.newtype.pricemodule.presenter.MarketHKOrUSAStockFragmentPresenter;
import com.newtype.pricemodule.presenter.contract.MarketHKOrUSAStockFragmentContract;

/**
 * Created by Popper on 2019/4/8.
 */
@Route(path = ARouterManager.PriceModule_MarketHKOrUSAStockFragment)
public class MarketHKOrUSAStockFragment extends LazyFragment<MarketHKOrUSAStockFragmentPresenter> implements MarketHKOrUSAStockFragmentContract.View {
    @Override
    protected void initView(View view) {
        TextView textView = view.findViewById(R.id.textView);
        Bundle arguments = getArguments();
        int type = arguments.getInt(Config.MARKET_STOCK_TYPE);
        textView.setText(0 == type ? "这个是港股的Fragment" : "这个是美股的Fragment");
    }

    @Override
    public MarketHKOrUSAStockFragmentPresenter getPresentInstance() {
        return new MarketHKOrUSAStockFragmentPresenter( getContext());
    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void initData() {

    }

    @Override
    public int getLayoutId() {
        return R.layout.price_fragment_market_hk_stock;
    }
}
