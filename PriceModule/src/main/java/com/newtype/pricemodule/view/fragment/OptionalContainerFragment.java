package com.newtype.pricemodule.view.fragment;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.newtype.basemodule.constants.ARouterManager;
import com.newtype.basemodule.base.view.LazyFragment;
import com.newtype.basemodule.model.http.api.TradingService;
import com.newtype.pricemodule.R;
import com.newtype.pricemodule.adapter.OptionalViewPagerAdapter;
import com.newtype.pricemodule.presenter.OptionalContainerFragmentPresenter;
import com.newtype.pricemodule.presenter.contract.OptionalContainerFragmentContract;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.ColorTransitionPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.SimplePagerTitleView;

import java.util.ArrayList;

/**
 * Created by Popper on 2019/4/7.
 */
@Route(path = ARouterManager.PriceModule_OptionalFragment)
public class OptionalContainerFragment extends LazyFragment<OptionalContainerFragmentPresenter> implements ViewPager.OnPageChangeListener, OptionalContainerFragmentContract.View {

    private MagicIndicator mMagicIndicator;
    private ArrayList<String> mList;
    private ViewPager mViewPager;
    private ArrayList<Fragment> mFragmentList;

    @Override
    public int getLayoutId() {
        return R.layout.price_fragment_optional;
    }

    @Override
    protected void initListener() {
        mViewPager.addOnPageChangeListener(this);
    }

    @Override
    protected void initData() {
        mList = new ArrayList<>();
        mList.add(getString(R.string.all));
        mList.add(getString(R.string.hk_stock));
        mList.add(getString(R.string.usa_stock));
        mList.add(getString(R.string.hk_future));
        mList.add(getString(R.string.international_future));
        mList.add(getString(R.string.forex_market));
        mFragmentList = new ArrayList<>();
        for (int i = 0; i < mList.size(); i++) {
            mFragmentList.add((Fragment) ARouter.getInstance().build(ARouterManager.PriceModule_OptionalContentFragment).navigation());
        }
        initMagicIndicator();
        OptionalViewPagerAdapter optionalViewPagerAdapter = new OptionalViewPagerAdapter(getChildFragmentManager(), mFragmentList);
        mViewPager.setAdapter(optionalViewPagerAdapter);
    }

    @Override
    protected void initView(View view) {
        mMagicIndicator = view.findViewById(R.id.mIndicator);
        mViewPager = view.findViewById(R.id.view_pager);
    }

    @Override
    public OptionalContainerFragmentPresenter getPresentInstance() {
        return new OptionalContainerFragmentPresenter( getContext());
    }

    private void initMagicIndicator() {
        CommonNavigator commonNavigator = new CommonNavigator(getContext());
//        commonNavigator.setAdjustMode(true);
        commonNavigator.setSkimOver(true);
        commonNavigator.setAdapter(new CommonNavigatorAdapter() {
            @Override
            public int getCount() {
                return mList == null ? 0 : mList.size();
            }

            @Override
            public IPagerTitleView getTitleView(Context context, final int index) {
                SimplePagerTitleView simplePagerTitleView = new ColorTransitionPagerTitleView(context);
                simplePagerTitleView.setNormalColor(Color.parseColor("#8E9AB6"));
                simplePagerTitleView.setSelectedColor(Color.parseColor("#3891FF"));
                simplePagerTitleView.setText(mList.get(index));
                simplePagerTitleView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mViewPager.setCurrentItem(index);
                        mMagicIndicator.onPageSelected(index);
                        mMagicIndicator.onPageScrolled(index, 0.0f, 0);
                    }
                });
                return simplePagerTitleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                LinePagerIndicator linePagerIndicator = new LinePagerIndicator(context);
                linePagerIndicator.setMode(LinePagerIndicator.MODE_WRAP_CONTENT);
                linePagerIndicator.setColors(Color.parseColor("#3891FF"));
                return linePagerIndicator;
            }
        });
        mMagicIndicator.setNavigator(commonNavigator);
        ViewPagerHelper.bind(mMagicIndicator, mViewPager);
    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {

    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }
}
