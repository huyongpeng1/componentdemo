package com.newtype.pricemodule.presenter;

import android.content.Context;

import com.newtype.basemodule.base.presenter.RXPresenter;
import com.newtype.basemodule.model.http.api.TradingService;
import com.newtype.pricemodule.model.mode.MarketForexFragmentMode;
import com.newtype.pricemodule.presenter.contract.MarketForexFragmentContract;

/**
 * Created by Popper on 2019/4/9.
 */

public class MarketForexFragmentPresenter extends RXPresenter<MarketForexFragmentContract.View, MarketForexFragmentMode> implements MarketForexFragmentContract.Presenter {

    public MarketForexFragmentPresenter( Context context) {
        super( context);
    }

    @Override
    public MarketForexFragmentMode getModeInstance() {
        return new MarketForexFragmentMode();
    }
}
