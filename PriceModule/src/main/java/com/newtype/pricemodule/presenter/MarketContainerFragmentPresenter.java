package com.newtype.pricemodule.presenter;

import android.content.Context;

import com.newtype.basemodule.base.presenter.RXPresenter;
import com.newtype.basemodule.model.http.api.TradingService;
import com.newtype.pricemodule.model.mode.MarketContainerFragmentMode;
import com.newtype.pricemodule.presenter.contract.MarketContainerFragmentContract;

/**
 * Created by Popper on 2019/4/9.
 */

public class MarketContainerFragmentPresenter extends RXPresenter<MarketContainerFragmentContract.View,MarketContainerFragmentMode> implements MarketContainerFragmentContract.Presenter {

    public MarketContainerFragmentPresenter( Context context) {
        super( context);
    }

    @Override
    public MarketContainerFragmentMode getModeInstance() {
        return new MarketContainerFragmentMode();
    }
}
