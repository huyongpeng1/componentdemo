package com.newtype.pricemodule.presenter;

import android.content.Context;

import com.newtype.basemodule.base.presenter.RXPresenter;
import com.newtype.basemodule.model.http.api.TradingService;
import com.newtype.basemodule.utils.ToastUtil;
import com.newtype.pricemodule.model.mode.PriceContainerFragmentMode;
import com.newtype.pricemodule.presenter.contract.PriceContainerFragmentContract;

/**
 * Created by Popper on 2019/4/9.
 */

public class PriceContainerFragmentPresenter extends RXPresenter<PriceContainerFragmentContract.View, PriceContainerFragmentMode> implements PriceContainerFragmentContract.Presenter {

    public PriceContainerFragmentPresenter( Context context) {
        super( context);
    }

    public void showToast() {
        ToastUtil.showToastCenter("HAHAAA");
    }

    @Override
    public PriceContainerFragmentMode getModeInstance() {
        return new PriceContainerFragmentMode();
    }
}
