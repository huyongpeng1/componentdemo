package com.newtype.pricemodule.presenter;

import android.content.Context;

import com.newtype.basemodule.base.presenter.RXPresenter;
import com.newtype.basemodule.model.http.api.TradingService;
import com.newtype.pricemodule.model.mode.OptionalContainerFragmentMode;
import com.newtype.pricemodule.presenter.contract.OptionalContainerFragmentContract;

/**
 * Created by Popper on 2019/4/9.
 */

public class OptionalContainerFragmentPresenter extends RXPresenter<OptionalContainerFragmentContract.View, OptionalContainerFragmentMode> implements OptionalContainerFragmentContract.Presenter {

    public OptionalContainerFragmentPresenter( Context context) {
        super( context);
    }

    @Override
    public OptionalContainerFragmentMode getModeInstance() {
        return new OptionalContainerFragmentMode();
    }
}
