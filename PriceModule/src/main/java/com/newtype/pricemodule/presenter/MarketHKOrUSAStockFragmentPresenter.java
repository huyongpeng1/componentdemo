package com.newtype.pricemodule.presenter;

import android.content.Context;

import com.newtype.basemodule.base.presenter.RXPresenter;
import com.newtype.basemodule.model.http.api.TradingService;
import com.newtype.pricemodule.model.mode.MarketHKOrUSAStockFragmentMode;
import com.newtype.pricemodule.presenter.contract.MarketHKOrUSAStockFragmentContract;

/**
 * Created by Popper on 2019/4/9.
 */

public class MarketHKOrUSAStockFragmentPresenter extends RXPresenter<MarketHKOrUSAStockFragmentContract.View, MarketHKOrUSAStockFragmentMode> implements MarketHKOrUSAStockFragmentContract.Presenter {

    public MarketHKOrUSAStockFragmentPresenter( Context context) {
        super(context);
    }

    @Override
    public MarketHKOrUSAStockFragmentMode getModeInstance() {
        return new MarketHKOrUSAStockFragmentMode();
    }
}
