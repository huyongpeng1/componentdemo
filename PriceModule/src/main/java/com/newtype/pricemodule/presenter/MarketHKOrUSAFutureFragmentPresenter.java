package com.newtype.pricemodule.presenter;

import android.content.Context;

import com.newtype.basemodule.base.presenter.RXPresenter;
import com.newtype.basemodule.model.http.api.TradingService;
import com.newtype.pricemodule.model.mode.MarketHKOrUSAFutureFragmentMode;
import com.newtype.pricemodule.presenter.contract.MarketHKOrUSAFutureFragmentContract;

/**
 * Created by Popper on 2019/4/9.
 */

public class MarketHKOrUSAFutureFragmentPresenter extends RXPresenter<MarketHKOrUSAFutureFragmentContract.View, MarketHKOrUSAFutureFragmentMode> implements MarketHKOrUSAFutureFragmentContract.Presenter {

    public MarketHKOrUSAFutureFragmentPresenter( Context context) {
        super( context);
    }

    @Override
    public MarketHKOrUSAFutureFragmentMode getModeInstance() {
        return new MarketHKOrUSAFutureFragmentMode();
    }
}
