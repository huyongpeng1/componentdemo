package com.newtype.pricemodule.presenter;

import android.content.Context;
import android.util.Log;

import com.newtype.basemodule.base.presenter.RXPresenter;
import com.newtype.basemodule.model.http.api.TradingService;
import com.newtype.pricemodule.model.mode.OptionalContentFragmentMode;
import com.newtype.pricemodule.presenter.contract.OptionalContentFragmentContract;

/**
 * Created by Popper on 2019/4/9.
 */

public class OptionalContentFragmentPresenter extends RXPresenter<OptionalContentFragmentContract.View, OptionalContentFragmentMode> implements OptionalContentFragmentContract.Presenter {

    public OptionalContentFragmentPresenter( Context context) {
        super( context);
    }

    @Override
    public OptionalContentFragmentMode getModeInstance() {
        return new OptionalContentFragmentMode();
    }

}
