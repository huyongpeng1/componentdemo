package com.newtype.basemodule.interfaces;

import android.view.View;

/**
 * Created by Popper on 2019/4/4.
 */
//多module 如果用switch判断id，会有问题，建议用switch去判断data
public interface OnItemClickListener<T> {
    void onItemClick(View view, T data, int position);
}
