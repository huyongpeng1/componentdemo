package com.newtype.basemodule.constants;

/**
 * Created by Popper on 2019/4/3.
 */

public class ARouterManager {
    //AppModule
    public static final String MainModule_MainActivity = "/Main/MainActivity";

    //PriceModule
    public static final String PriceModule_OptionalFragment = "/PriceModule/OptionalFragment";
    public static final String PriceModule_OptionalContentFragment = "/PriceModule/OptionalContentFragment";
    public static final String PriceModule_MarketContainerFragment = "/PriceModule/MarketContainerFragment";
    public static final String PriceModule_MarketHKOrUSAStockFragment = "/PriceModule/MarketHKOrUSAStockFragment";
    public static final String PriceModule_MarketHKOrUSAFutureFragment = "/PriceModule/MarketHKOrUSAFutureFragment";
    public static final String PriceModule_MarketForexFragment = "/PriceModule/MarketForexFragment";
    public static final String PriceModule_PriceContainerFragment = "/PriceModule/PriceContainerFragment";

    //TradeModule
    public static final String TradeModule_TradeStockFragment = "/TradeModule/TradeStockFragment";
    public static final String TradeModule_InternationalFutureFragment = "/TradeModule/InternationalFutureFragment";
    public static final String TradeModule_ForexFragment = "/TradeModule/ForexFragment";
    public static final String TradeModule_TradeContainerFragment = "/TradeModule/TradeContainerFragment";

    //MemberCenterModule
    public static final String MemberCenterModule_MemberFragment = "/MemberCenterModule/MemberFragment";

    //AboutUsModule
    public static final String AboutUsModule_AboutUsFragment = "/AboutUsModule/AboutUsFragment";


    //LoginModule
    public static final String LoginModule_LoginActivity = "/LoginModule/LoginActivity";
}
