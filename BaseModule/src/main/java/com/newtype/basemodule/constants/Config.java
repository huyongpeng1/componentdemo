package com.newtype.basemodule.constants;

/**
 * Created by Popper on 2019/4/7.
 */

public class Config {
    public static final boolean debug = true;
    //港股：0  美股：1
    public static final String TRADE_STOCK_TYPE = "trade_stock_type";

    //港股：0  美股：1
    public static final String PRICE_STOCK_TYPE = "price_stock_type";

    //香港期货：0  国际期货：1
    public static final String MARKET_FUTURE_TYPE = "market_future_type";

    //港股：0  美股：1
    public static final String MARKET_STOCK_TYPE = "market_stock_type";

    //网络请求相关
    public static final int CONNECT_TIME_OUT = 5;
    public static final int READ_TIME_OUT = 30;
    public static final String BASE_URL = "https://www.baidu.com/";
    public static final String SUCCESS = "success";
}
