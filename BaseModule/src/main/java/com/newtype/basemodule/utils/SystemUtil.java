package com.newtype.basemodule.utils;

import android.content.Context;
import android.net.ConnectivityManager;

import com.newtype.basemodule.base.application.BaseApplication;

/**
 * Created by Popper on 2019/4/12.
 */

public class SystemUtil {
    public static boolean isNetworkConnected() {
        ConnectivityManager connectivityManager = (ConnectivityManager) BaseApplication.getContext().getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager.getActiveNetworkInfo() != null;
    }
}
