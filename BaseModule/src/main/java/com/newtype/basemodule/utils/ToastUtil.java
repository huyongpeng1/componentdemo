package com.newtype.basemodule.utils;

import android.view.Gravity;
import android.widget.Toast;

import com.newtype.basemodule.base.application.BaseApplication;

/**
 * Created by admin on 2017/3/22.
 */

public class ToastUtil {
    private static Toast mToast;

    public static void showToastCenter(String string) {
        if (mToast == null) {
            synchronized (ToastUtil.class) {
                if (mToast == null) {
                    mToast = Toast.makeText(BaseApplication.getContext(), string, Toast.LENGTH_SHORT);
                    mToast.setGravity(Gravity.CENTER, 0, 0);
                }
            }
        } else {
            mToast.setText(string);
        }
        mToast.show();
    }

    public static void showToastDefault(String s) {
        if (mToast == null) {
            synchronized (ToastUtil.class) {
                if (mToast == null) {
                    mToast = Toast.makeText(BaseApplication.getContext(), s, Toast.LENGTH_SHORT);
                }
            }
        } else {
            mToast.setText(s);
        }
        mToast.show();
    }

    public static void destoryToast() {
        if (mToast != null) {
            mToast = null;
        }
    }
}
