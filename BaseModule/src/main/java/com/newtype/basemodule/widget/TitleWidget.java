package com.newtype.basemodule.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.newtype.basemodule.R;
import com.newtype.basemodule.interfaces.OnItemClickListener;
import com.newtype.basemodule.utils.ToastUtil;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.CommonPagerTitleView;

import java.util.List;

/**
 * Created by Popper on 2019/4/4.
 */

public class TitleWidget extends RelativeLayout {

    private boolean mLeftVisiable;
    private boolean mRightVisible;
    private int mLeftResourceId;
    private int mRightResourceId;
    private List<String> mList;
    private OnItemClickListener<String> mItemClickListener;
    private MagicIndicator mMagicIndicator;
    private boolean mIfHaveUnderLine;
    private ViewPager mViewPager;

    public TitleWidget(Context context) {
        super(context);
    }

    public TitleWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.widget_title, this);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.TitleWidget, 0, 0);
        mLeftVisiable = typedArray.getBoolean(R.styleable.TitleWidget_leftVisible, true);
        mRightVisible = typedArray.getBoolean(R.styleable.TitleWidget_rightVisible, true);
        mLeftResourceId = typedArray.getResourceId(R.styleable.TitleWidget_leftImg, 0);
        mRightResourceId = typedArray.getResourceId(R.styleable.TitleWidget_rightImg, 0);
        mIfHaveUnderLine = typedArray.getBoolean(R.styleable.TitleWidget_ifHaveUnderLine, true);
        setUpView();
        typedArray.recycle();
    }

    public void setMIndicatorPosition() {
        MagicIndicator magicIndicator = findViewById(R.id.mi_header_mi);
        RelativeLayout.LayoutParams layoutParams = (LayoutParams) magicIndicator.getLayoutParams();
        layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        layoutParams.setMargins((int) getContext().getResources().getDimension(R.dimen.dp_20), 0, 0, 0);
        magicIndicator.setLayoutParams(layoutParams);
    }

    private void setUpView() {
        ImageView leftImg = findViewById(R.id.iv_left);
        leftImg.setVisibility(mLeftVisiable ? VISIBLE : GONE);
        if (mLeftResourceId != 0) {
            leftImg.setImageDrawable(ContextCompat.getDrawable(getContext(), mLeftResourceId));
        }
        ImageView rightImg = findViewById(R.id.iv_right);
        rightImg.setVisibility(mRightVisible ? VISIBLE : GONE);
        if (mRightResourceId != 0) {
            rightImg.setImageDrawable(ContextCompat.getDrawable(getContext(), mRightResourceId));
        }
        mMagicIndicator = findViewById(R.id.mi_header_mi);
        initMIndicator();

    }

    private void initMIndicator() {
        CommonNavigator commonNavigator = new CommonNavigator(getContext());
//        commonNavigator.setAdjustMode(true);
        commonNavigator.setSkimOver(true);
        commonNavigator.setAdapter(mCommonNavigatorAdapter);
        mMagicIndicator.setNavigator(commonNavigator);
    }

    public void setOnTitleClickListener(OnItemClickListener<String> onItemClickListener) {
        mItemClickListener = onItemClickListener;
        ImageView leftImg = findViewById(R.id.iv_left);
        leftImg.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mItemClickListener != null) {
                    Log.d("way", "onClick: left");
                    mItemClickListener.onItemClick(v, "left", -1);
                }
            }
        });
        ImageView rightImg = findViewById(R.id.iv_right);
        rightImg.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mItemClickListener != null) {
                    Log.d("way", "onClick: right");
                    mItemClickListener.onItemClick(v, "right", -1);
                }
            }
        });
    }

    //用于不跟ViewPager关联
    public void setList(List<String> list/*, OnItemClickListener<String> onItemClickListener*/) {
        setList(list, /*onItemClickListener,*/ null);
    }

    //用于跟ViewPager关联
    public void setList(List<String> list,/* OnItemClickListener<String> onItemClickListener,*/ ViewPager viewPager) {
        mList = list;
//        mItemClickListener = onItemClickListener;
//        mCommonNavigatorAdapter.notifyDataSetChanged();
        initMIndicator();
        mViewPager = viewPager;
        if (viewPager != null)
            ViewPagerHelper.bind(mMagicIndicator, viewPager);
    }


    CommonNavigatorAdapter mCommonNavigatorAdapter = new CommonNavigatorAdapter() {
        @Override
        public int getCount() {
            return mList == null ? 0 : mList.size();
        }

        @Override
        public IPagerTitleView getTitleView(final Context context, final int index) {
            CommonPagerTitleView commonPagerTitleView = new CommonPagerTitleView(context);
            commonPagerTitleView.setContentView(R.layout.base_view_title_mi);
            final TextView rvTitleContent = commonPagerTitleView.findViewById(R.id.tv_title_content);
            rvTitleContent.setText(mList.get(index));
            commonPagerTitleView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                  /*  if (mItemClickListener != null) {
                        Log.d("way", "onClick: tab");
                        mItemClickListener.onItemClick(v, "tab", index);
                    }*/
                    if (mViewPager != null) {
                        mViewPager.setCurrentItem(index);
                    }
                    mMagicIndicator.onPageSelected(index);
                    mMagicIndicator.onPageScrolled(index, 0.0f, 0);
                }
            });

            commonPagerTitleView.setOnPagerTitleChangeListener(new CommonPagerTitleView.OnPagerTitleChangeListener() {
                @Override
                public void onSelected(int i, int i1) {
                    rvTitleContent.setTextColor(context.getResources().getColor(R.color.colorffffff));
                    rvTitleContent.setTextSize(19);
                }

                @Override
                public void onDeselected(int i, int i1) {
                    rvTitleContent.setTextColor(context.getResources().getColor(R.color.color939393));
                    rvTitleContent.setTextSize(16);
                }

                @Override
                public void onLeave(int i, int i1, float v, boolean b) {

                }

                @Override
                public void onEnter(int i, int i1, float v, boolean b) {

                }
            });
            return commonPagerTitleView;
         /*   SimplePagerTitleView simplePagerTitleView = new ColorTransitionPagerTitleView(context);
            simplePagerTitleView.setNormalColor(getContext().getResources().getColor(R.color.color8E9AB6));
            simplePagerTitleView.setSelectedColor(getContext().getResources().getColor(R.color.colorffffff));
            simplePagerTitleView.setText(mList.get(index));
            simplePagerTitleView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    mViewPager.setCurrentItem(index);
                    if (mItemClickListener != null) {
                        mItemClickListener.onItemClick(v, index, index);
                    }
                    if (mViewPager != null) {
                        mViewPager.setCurrentItem(index);
                    } else {
                        mMagicIndicator.onPageSelected(index);
                        mMagicIndicator.onPageScrolled(index, 0.0f, 0);
                    }
                }
            });
            return simplePagerTitleView;
        */
        }

        @Override
        public IPagerIndicator getIndicator(Context context) {
            if (mIfHaveUnderLine) {
                LinePagerIndicator linePagerIndicator = new LinePagerIndicator(context);
                linePagerIndicator.setMode(LinePagerIndicator.MODE_WRAP_CONTENT);
                linePagerIndicator.setColors(getContext().getResources().getColor(R.color.color3891FF));
                return linePagerIndicator;
            }
            return null;
        }
    };

}
