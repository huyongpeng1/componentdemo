package com.newtype.basemodule.widget;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import com.newtype.basemodule.R;
import com.newtype.basemodule.interfaces.OnItemClickListener;
import com.newtype.basemodule.utils.AtyContainer;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

/**
 * Created by Popper on 2019/4/9.
 */

public class DialogShow {
    public static void showNetworkDialog(final Context context) {
        //Button [Neutral] [Negative] [Positive]
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("网络连接不可用,请设置网络");
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                AtyContainer.getInstance().ExitApp();
            }
        });
        builder.setPositiveButton("数据网络设置", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // 跳转到系统的数据网络设置界面
//                Intent intent = new Intent(Settings.ACTION_DATA_ROAMING_SETTINGS);
//                context.startActivity(intent);
                Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                context.startActivity(intent);
            }
        });
//        builder.setPositiveButton("无线网络设置", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//                // 跳转到系统的wifi网络设置界面
//                Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
//                context.startActivity(intent);
//            }
//        });
        builder.show();
    }
    public static void showSelectPhoneTypeDialog(final Activity activity, View targetView, final OnItemClickListener<Integer> onItemClickListener) {
        View view = LayoutInflater.from(activity).inflate(R.layout.dialog_select_phone_type, null);
        LinearLayout llChineseMainland = view.findViewById(R.id.ll_chinese_mainland);
        LinearLayout llChineseHK = view.findViewById(R.id.ll_chinese_hk);
        int width = (int) activity.getResources().getDimension(R.dimen.dialog_140_dp_dimension);
        final PopupWindow popupWindow = new PopupWindow(view,
                width, WRAP_CONTENT);

        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        /*final WindowManager.LayoutParams lp = activity.getWindow().getAttributes();
        lp.alpha = 0.5f;
        activity.getWindow().setAttributes(lp);*/
        activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        popupWindow.setFocusable(true);
        popupWindow.setAnimationStyle(R.style.style_scale_in_popup);
        popupWindow.setOutsideTouchable(true);
//        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);
        popupWindow.showAsDropDown(targetView, 0, 0, Gravity.BOTTOM);
        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
          /*      lp.alpha = 1f;
                activity.getWindow().setAttributes(lp);*/
                popupWindow.dismiss();
            }
        });
        llChineseMainland.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
          /*      lp.alpha = 1f;
                activity.getWindow().setAttributes(lp);*/
                popupWindow.dismiss();
                if (onItemClickListener != null)
                    onItemClickListener.onItemClick(v, 0, 0);
            }
        });
        llChineseHK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             /*   lp.alpha = 1f;
                activity.getWindow().setAttributes(lp);*/
                popupWindow.dismiss();
                if (onItemClickListener != null)
                    onItemClickListener.onItemClick(v, 1, 1);
            }
        });

    }
}
