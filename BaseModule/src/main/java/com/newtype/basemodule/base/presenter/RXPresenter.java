package com.newtype.basemodule.base.presenter;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;


import com.newtype.basemodule.base.model.BaseMode;
import com.newtype.basemodule.base.view.BaseView;
import com.newtype.basemodule.model.http.ProgressSubscriber;
import com.newtype.basemodule.model.http.RequestListener;
import com.newtype.basemodule.model.http.api.TradingService;
import com.newtype.basemodule.model.http.upload.interfaces.UploadProgressListener;
import com.newtype.basemodule.model.http.upload.requestbody.ProgressRequestBody;

import java.io.File;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;


/**
 * Created by popper on 2017/11/20.
 */

public abstract class RXPresenter<T extends BaseView, K extends BaseMode> implements BasePresenter<T, K> {

    public T mView;
    public K mMode;
    public Context mContext;

    public RXPresenter( Context context) {
        mContext = context;
    }

    @Override
    public void attachView(T view) {
        mView = view;
        mMode = getModeInstance();
    }

    @Override
    public void detachView() {
        mView = null;
        if (mMode != null) {
            mMode.unSubscribe();
        }
        mMode = null;
        mContext = null;
    }


}
