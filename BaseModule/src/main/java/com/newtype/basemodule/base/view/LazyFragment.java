package com.newtype.basemodule.base.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.newtype.basemodule.base.presenter.BasePresenter;
import com.newtype.basemodule.utils.ToastUtil;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Popper on 2019/4/7.
 */

public abstract class LazyFragment<T extends BasePresenter> extends Fragment implements BaseView {
    public T mPresenter;
    private View mView;
    private Unbinder mUnBinder;
    public FragmentActivity mActivity;
    private boolean mIsViewCreate;//View加载完毕的标志
    private boolean mIsUIVisible;//Fragment 对用户是否可见

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(getLayoutId(), null);
        return mView;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mUnBinder = ButterKnife.bind(this, view);
        mPresenter = getPresentInstance();
        if (mPresenter != null) {
            mPresenter.attachView(this);
        }
        initView(mView);
        mActivity = getActivity();
        mIsViewCreate = true;
        lazyLoad();
        initListener();
    }

    private void lazyLoad() {
        //这里进行双重标记判断,是因为setUserVisibleHint会多次回调,并且会在onCreateView执行前回调,必须确保onCreateView加载完毕且页面可见,才加载数据
        if (mIsViewCreate && mIsUIVisible) {
            initData();
            //数据加载完毕,恢复标记,防止重复加载
            mIsViewCreate = false;
            mIsUIVisible = false;

        }
    }

    //在ViewPager里面放置Fragment会执行的方法
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            mIsUIVisible = true;
            lazyLoad();
        } else {
            mIsUIVisible = false;
        }
    }

    //开启事务替换会执行的方法
    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!isHidden()) {
            mIsUIVisible = true;
            lazyLoad();
        } else {
            mIsUIVisible = false;
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIsViewCreate = false;
        mIsUIVisible = false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ToastUtil.destoryToast();
        mUnBinder.unbind();
        if (mPresenter != null) {
            mPresenter.detachView();
        }
    }


    protected abstract void initListener();

    protected abstract void initData();

    protected abstract void initView(View view);

    public abstract T getPresentInstance();

    public abstract int getLayoutId();
}
