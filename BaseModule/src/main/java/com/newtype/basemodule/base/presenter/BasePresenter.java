package com.newtype.basemodule.base.presenter;

import com.newtype.basemodule.base.model.BaseMode;
import com.newtype.basemodule.base.view.BaseView;

/**
 * Created by popper on 2017/11/20.
 */

public interface BasePresenter<T extends BaseView, K extends BaseMode> {
    void attachView(T view);

    void detachView();

    K getModeInstance();

}
