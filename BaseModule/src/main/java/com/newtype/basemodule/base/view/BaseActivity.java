package com.newtype.basemodule.base.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.newtype.basemodule.R;
import com.newtype.basemodule.base.presenter.BasePresenter;
import com.newtype.basemodule.utils.AtyContainer;
import com.newtype.basemodule.utils.ToastUtil;
import com.newtype.basemodule.widget.StatusBarCompat;

import org.greenrobot.eventbus.EventBus;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by popper on 2017/11/20.
 */

public abstract class BaseActivity<T extends BasePresenter> extends AppCompatActivity implements BaseView {

    public T mPresenter;
    private Unbinder mBind;
    public View mView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StatusBarCompat.compat(this, getResources().getColor(R.color.color000000));
        mView = LayoutInflater.from(this).inflate(getLayoutId(), null);
        setContentView(mView);
        mBind = ButterKnife.bind(this);
        initInject();
        mPresenter = getPresentInstance();
        if (mPresenter != null) {
            mPresenter.attachView(this);
        }
        AtyContainer.getInstance().addActivity(this);
        if (initEventBus()) {
            EventBus.getDefault().register(this);
        }
        initView();
        initData();
        initListener();
    }

    protected boolean initEventBus() {
        return false;
    }

   /* public ActivityComponent getActivityComponent() {
        return DaggerActivityComponent.builder().appModule(new AppModule()).activityModule(new ActivityModule(this)).build();
    }*/

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ToastUtil.destoryToast();
        mBind.unbind();
        if (initEventBus()) {
            EventBus.getDefault().unregister(this);
        }
        AtyContainer.getInstance().removeActivity(this);
    }


    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
//        overridePendingTransition(android.R.anim.scale_in, android.R.anim.scale_out);
    }

    @Override
    public void finish() {
        super.finish();
//        overridePendingTransition(android.R.anim.scale_in, android.R.anim.scale_out);
    }

    public String getEditTextContent(EditText editText) {
        return editText.getText().toString().trim();
    }

    public String getTextContent(TextView textView) {
        return textView.getText().toString().trim();
    }

    protected abstract void initInject();

    protected abstract void initListener();

    protected abstract void initData();

    protected abstract void initView();

    public abstract int getLayoutId();

    public abstract T getPresentInstance();
}
