package com.newtype.basemodule.base.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NavUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.newtype.basemodule.base.presenter.BasePresenter;
import com.newtype.basemodule.utils.ToastUtil;

import java.lang.reflect.Field;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by popper on 2017/11/21.
 */

public abstract class BaseFragment<T extends BasePresenter> extends Fragment implements BaseView {
    private Unbinder mBind;
    public FragmentActivity mActivity;
    public T mPresenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mActivity = getActivity();
        View inflate = inflater.inflate(getLayoutId(), container, false);
        mPresenter = getPresentInstance();
        return inflate;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBind = ButterKnife.bind(this, view);
        if (mPresenter != null) {
            mPresenter.attachView(this);
        }
        initView();
        initData();
        initListener();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ToastUtil.destoryToast();
        mBind.unbind();
        if (mPresenter != null) {
            mPresenter.detachView();
        }
    }

    protected abstract int getLayoutId();

    protected abstract void initListener();

    protected abstract void initData();

    protected abstract void initView();

    public abstract T getPresentInstance();
}
