package com.newtype.basemodule.base.application;

import android.app.Application;

import com.alibaba.android.arouter.launcher.ARouter;
import com.newtype.basemodule.constants.Config;

/**
 * Created by popper on 2017/11/20.
 */

public class BaseApplication extends Application {
    public static BaseApplication instance;


    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        if (Config.debug) {
            ARouter.openDebug();
            ARouter.openLog();
            ARouter.init(this);
        }
    }

    public static BaseApplication getContext() {
        return instance;
    }


}
