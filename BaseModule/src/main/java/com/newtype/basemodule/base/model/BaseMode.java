package com.newtype.basemodule.base.model;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;


import com.newtype.basemodule.model.http.ProgressSubscriber;
import com.newtype.basemodule.model.http.RequestListener;
import com.newtype.basemodule.model.http.upload.interfaces.UploadProgressListener;
import com.newtype.basemodule.model.http.upload.requestbody.ProgressRequestBody;
import com.newtype.basemodule.utils.ToastUtil;

import java.io.File;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by popper on 2019-04-10.
 */

public abstract class BaseMode {

    private CompositeSubscription mCompositeSubscription;


    public void addSubscribe(Subscription subscription) {
        if (mCompositeSubscription == null) {
            mCompositeSubscription = new CompositeSubscription();
        }
        mCompositeSubscription.add(subscription);
    }

    public void unSubscribe() {
        if (mCompositeSubscription != null) {
            mCompositeSubscription.unsubscribe();
        }
    }

    //默认progress显示正在加载中...
    public void doHttpDeal(Context context, Observable observable, RequestListener requestListener) {
        ProgressSubscriber progressSubscriber = new ProgressSubscriber(context, requestListener);
        Subscription subscribe = observable
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(progressSubscriber);
        addSubscribe(subscribe);
    }

    //默认progress显示正在加载中...
    public void doHttpDeal1(Context context, Observable observable, RequestListener requestListener) {
        ProgressSubscriber progressSubscriber = new ProgressSubscriber(context, requestListener);
        Subscription subscribe = observable
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
//                .map(progressSubscriber)
                .subscribe(progressSubscriber);
        addSubscribe(subscribe);
    }

    //progress显示的内容可以自定义
    public void doHttpDeal(Context context, Observable observable, RequestListener requestListener, String msg) {
        ProgressSubscriber progressSubscriber = new ProgressSubscriber(context, requestListener, msg);
        Subscription subscribe = observable
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(progressSubscriber);
        addSubscribe(subscribe);
    }

    //progress显示内容可以自定义，并且自己控制是否显示进度条
    public void doHttpDeal(Context context, Observable observable, RequestListener requestListener, boolean isShow) {
        ProgressSubscriber progressSubscriber = new ProgressSubscriber(context, requestListener, isShow);
        Subscription subscribe = observable
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(progressSubscriber);
        addSubscribe(subscribe);
    }

    //上传文件（图片）和参数
    public MultipartBody.Part getUploadFile(Context context, File file, String msg) {
        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setMessage(msg);
        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part part = MultipartBody.Part.createFormData("file_name", file.getName(), new ProgressRequestBody(requestBody, new UploadProgressListener() {
            @Override
            public void onProgress(final long currentCount, final long totalCount) {
                Observable.just(currentCount)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Action1<Long>() {
                            @Override
                            public void call(Long aLong) {
                                progressDialog.setMax((int) totalCount);
                                progressDialog.setProgress((int) currentCount);
                                progressDialog.show();
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (currentCount == totalCount) {
                                            progressDialog.dismiss();
                                        }
                                    }
                                }, 1200);
                            }
                        });
            }
        }));
        return part;
    }

    public HashMap<String, RequestBody> getUploadParameter(HashMap<String, String> map) {
        HashMap<String, RequestBody> hashMap = new HashMap<>();
        for (String key : map.keySet()) {
            RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), map.get(key));
            hashMap.put(key, requestBody);
        }
        return hashMap;
    }
}
