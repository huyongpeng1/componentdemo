package com.newtype.basemodule.model.http.upload.interfaces;

/**
 * Created by popper on 2017/11/24.
 */

public interface UploadProgressListener {
    void onProgress(long currentCount, long totalCount);
}
