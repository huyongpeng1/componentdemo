package com.newtype.basemodule.model.http;



import com.newtype.basemodule.constants.Config;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by popper on 2017/4/6.
 * Retrofitmanager用于创建retrofit
 * 使用RetrofitManager.getContext().create();
 */

public class RetrofitServiceManager {
    private Retrofit mRetrofit;
    private static RetrofitServiceManager mManager;

    public RetrofitServiceManager() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(Config.CONNECT_TIME_OUT, TimeUnit.SECONDS);
        builder.readTimeout(Config.READ_TIME_OUT, TimeUnit.SECONDS);
        // 添加公共参数拦截器
        if (Config.debug) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addNetworkInterceptor(interceptor);
        }
        mRetrofit = new Retrofit.Builder()
                .client(builder.build())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(Config.BASE_URL)
                .build();
    }


    /**
     * 获取RetrofitServiceManager
     *
     * @return
     */
    public static RetrofitServiceManager getInstance() {
        if (mManager == null) {
            synchronized (RetrofitServiceManager.class) {
                if (mManager == null) {
                    mManager = new RetrofitServiceManager();
                }
            }
        }
        return mManager;
    }

    /**
     * 获取对应的Service
     *
     * @param service Service 的 class
     */
    public <T> T create(Class<T> service) {
        return mRetrofit.create(service);
    }


}
