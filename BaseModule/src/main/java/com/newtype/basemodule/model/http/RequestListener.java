package com.newtype.basemodule.model.http;

/**
 * Created by popper on 2017/10/16.
 */

public interface RequestListener<T> {
     void onSuccess(T t);

     void onError(Throwable e);

}
