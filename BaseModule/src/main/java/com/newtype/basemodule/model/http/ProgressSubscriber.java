package com.newtype.basemodule.model.http;

import android.app.ProgressDialog;
import android.content.Context;


import com.newtype.basemodule.base.bean.BaseBean;
import com.newtype.basemodule.constants.Config;
import com.newtype.basemodule.utils.ToastUtil;

import java.net.ConnectException;
import java.net.SocketTimeoutException;

import rx.Subscriber;

/**
 * Created by popper on 2017/10/16.
 */

public class ProgressSubscriber<T> extends Subscriber<BaseBean<T>> /*implements Func1<BaseBean<T>, T>*/ {
    Context mContext;
    private ProgressDialog mProgressDialog;
    RequestListener mRequestListener;
    public boolean mIsShow = true;

    public ProgressSubscriber(Context context, RequestListener requestListener) {
        mContext = context;
        mRequestListener = requestListener;
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            //                        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            mProgressDialog.setMessage("正在加载中...");
        }
    }

    public ProgressSubscriber(Context context, RequestListener requestListener, String msg) {
        mContext = context;
        mRequestListener = requestListener;
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setMessage(msg);
        }
    }

    public ProgressSubscriber(Context context, RequestListener requestListener, boolean isShow) {
        mContext = context;
        mRequestListener = requestListener;
        mIsShow = isShow;
    }

    public void setHorizonAndProgress(int total, int current) {
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setMax(total);
        mProgressDialog.setProgress(current);
    }

    @Override
    public void onCompleted() {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }

    @Override
    public void onError(Throwable e) {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
        errorDo(e);
    }

    @Override
    public void onNext(BaseBean<T> baseBean) {
        if (Config.SUCCESS.equals(baseBean.code)) {
            mRequestListener.onSuccess(baseBean.data);
        } else {
            ToastUtil.showToastCenter(baseBean.msg);
        }
    }

    @Override
    public void onStart() {
        if (mProgressDialog != null && mIsShow)
            mProgressDialog.show();
    }

    private void errorDo(Throwable e) {
        if (e instanceof SocketTimeoutException) {
            ToastUtil.showToastDefault("网络链接超时，请检查您的网络状态");
        } else if (e instanceof ConnectException) {
            ToastUtil.showToastDefault("网络中断，请检查您的网络状态");
        } else {
            ToastUtil.showToastDefault("错误" + e.getMessage());
        }
        mRequestListener.onError(e);
    }
//这里采用的是map转换
  /*  @Override
    public T call(BaseBean<T> tBaseBean) {
        if (Constant.SUCCESS.equals(tBaseBean.code)) {
            return tBaseBean.userInfo;
        }
        Log.d("way", "ProgressSubscriber:请求失败了 ");
        ToastUtil.showToastCenter("请求失败了！");
        return tBaseBean.userInfo;
    }*/
}
