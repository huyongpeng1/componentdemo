package com.newtype.basemodule.model.http.api;


import com.newtype.basemodule.model.http.RetrofitServiceManager;
import com.newtype.basemodule.model.http.api.AboutUsApi;
import com.newtype.basemodule.model.http.api.MemberCenterApi;
import com.newtype.basemodule.model.http.api.PriceApi;
import com.newtype.basemodule.model.http.api.TradeApi;

/**
 * Created by popper on ${DATA}.
 */
//接口实例化统一放在这里
public class TradingService {

    public final PriceApi mPriceApi;
    public final TradeApi mTradeApi;
    public final MemberCenterApi mMemberCenterApi;
    public final AboutUsApi mAboutUsApi;
    private static TradingService sTradingService;

    public static TradingService getInstance() {
        if (sTradingService == null) {
            synchronized (TradingService.class) {
                if (sTradingService == null) {
                    sTradingService = new TradingService();
                }
            }

        }
        return sTradingService;
    }

    public TradingService() {
        mPriceApi = RetrofitServiceManager.getInstance().create(PriceApi.class);
        mTradeApi = RetrofitServiceManager.getInstance().create(TradeApi.class);
        mMemberCenterApi = RetrofitServiceManager.getInstance().create(MemberCenterApi.class);
        mAboutUsApi = RetrofitServiceManager.getInstance().create(AboutUsApi.class);
    }

 /*   public Observable<BaseBean<AccountLoginBean>> performLogin(String timestamp, String phoneNum, String edtPsd) {
        return mTradeService.accountLogin(timestamp, phoneNum, edtPsd, MacUtil.getMac(BaseApplication.getContext()));
    }*/
}
